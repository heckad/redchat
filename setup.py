from setuptools import find_packages
from setuptools import setup


def readme():
    with open('README.md', encoding='utf-8') as f:
        return f.read()


setup(
    name='redchat',
    version='0.0.1',
    description='Python chat bot builder.',
    long_description=readme(),
    author='Heckad',
    author_email='heckad@yandex.ru',
    packages=find_packages(exclude=['tests', 'tests.*']),
    license='MIT',
    keywords='chat bot api tools',
    classifiers=[
        'Development Status :: 1 - Production/Stable',
        'Programming Language :: Python :: 3.7',
        'Environment :: Console',
    ]
)
